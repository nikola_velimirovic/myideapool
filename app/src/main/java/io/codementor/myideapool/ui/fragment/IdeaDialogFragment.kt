package io.codementor.myideapool.ui.fragment

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.google.gson.Gson
import io.codementor.myideapool.R
import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.viewmodel.MainViewModel

class IdeaDialogFragment : DialogFragment() {
    private lateinit var viewModel: MainViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        viewModel = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)

        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle("Actions")
                .setItems(R.array.idea_actions) { _, which ->
                    val idea: Idea = Gson().fromJson(arguments?.getString(IDEA_KEY), Idea::class.java)
                    if (which == 0) {
                        viewModel.onUpdateIdeaAction(idea)
                    } else {
                        viewModel.onDeleteIdeaAction(idea)
                    }
                }
        return builder.create()
    }

    companion object {
        const val IDEA_KEY = "args.idea_key"

        fun create(idea: Idea): IdeaDialogFragment {
            val fragment = IdeaDialogFragment()

            val args = Bundle()
            args.putString(IDEA_KEY, Gson().toJson(idea))
            fragment.arguments = args

            return fragment
        }
    }
}