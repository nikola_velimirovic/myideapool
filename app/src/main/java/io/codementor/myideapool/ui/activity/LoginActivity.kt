package io.codementor.myideapool.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.Toast
import io.codementor.myideapool.R
import io.codementor.myideapool.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initUI()
        initViewModel()
    }

    fun initUI() {
        supportActionBar?.setTitle(R.string.app_name)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.setIcon(R.drawable.lightbulb)


        loginButton.setOnClickListener {
            val email = emailInput.text.toString()
            val password = passwordInput.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "Email cannot be empty.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                Toast.makeText(this, "Password cannot be empty.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            viewModel.onLogin(email, password)
        }

        val spannable = SpannableString(resources.getString(R.string.login_signup_text))
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.colorPrimary)), spannable.length - 17 , spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        signupText.text = spannable

        signupText.setOnClickListener {
            finish()
            startActivity(Intent(this, SignupActivity::class.java))
        }
    }

    fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)

        viewModel.loginResult.observe(this, Observer {resource ->
            if (resource != null && resource.isSuccessful) {
                finish()
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this, resource?.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}