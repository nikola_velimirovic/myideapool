package io.codementor.myideapool.ui.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.codementor.myideapool.R
import io.codementor.myideapool.entity.Idea
import kotlinx.android.synthetic.main.view_item_idea.view.*
import java.text.NumberFormat

class IdeaAdapter(private val callback: Callback) : PagedListAdapter<Idea, IdeaAdapter.IdeaViewHolder>(DIFF_CALLBACK) {
    private val formatter = NumberFormat.getNumberInstance()

    init {
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IdeaViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_item_idea, parent, false)
        return IdeaViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: IdeaViewHolder, position: Int) {
        val idea = getItem(position)
        if (idea != null) holder.bind(idea)
    }

    inner class IdeaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.more.setOnClickListener {
                getItem(adapterPosition)?.let { idea -> callback.onMore(idea) }
            }
        }

        fun bind(idea: Idea) {
            itemView.content.text = idea.content
            itemView.impact.text = idea.impact.toString()
            itemView.ease.text = idea.ease.toString()
            itemView.confidence.text = idea.confidence.toString()
            itemView.avg.text = formatter.format(idea.averageScore)
        }
    }

    interface Callback {
        fun onMore(idea: Idea)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Idea>() {
            override fun areContentsTheSame(oldItem: Idea, newItem: Idea): Boolean = oldItem == newItem

            override fun areItemsTheSame(oldItem: Idea, newItem: Idea): Boolean = oldItem.id == newItem.id
        }
    }
}