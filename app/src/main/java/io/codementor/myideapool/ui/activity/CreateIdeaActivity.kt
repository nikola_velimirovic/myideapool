package io.codementor.myideapool.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.Gson
import io.codementor.myideapool.R
import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_create_idea.*
import java.text.NumberFormat

class CreateIdeaActivity : AppCompatActivity() {
    private val formatter = NumberFormat.getNumberInstance()
    private lateinit var viewModel: MainViewModel
    private var ideaId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_idea)

        initUI()
        initViewModel()
    }

    private fun initUI() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setIcon(R.drawable.lightbulb)

        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 0

        val adapter = ArrayAdapter.createFromResource(this, R.array.idea_values, android.R.layout.simple_spinner_dropdown_item)
        impactSpinner.adapter = adapter
        easeSpinner.adapter = adapter
        confidenceSpinner.adapter = adapter

        val onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                calculateAvg()
            }
        }

        impactSpinner.onItemSelectedListener = onItemSelectedListener
        easeSpinner.onItemSelectedListener = onItemSelectedListener
        confidenceSpinner.onItemSelectedListener = onItemSelectedListener

        saveButton.setOnClickListener {
            val content = contentInput.text.toString()
            val impact = impactSpinner.selectedItem.toString().toInt()
            val ease = easeSpinner.selectedItem.toString().toInt()
            val confidence = confidenceSpinner.selectedItem.toString().toInt()

            viewModel.onSaveIdea(ideaId, content, impact, ease, confidence)
        }

        cancelButton.setOnClickListener {
            onBackPressed()
        }

        val ideaJson = intent.getStringExtra(IDEA_KEY)
        if (ideaJson != null) initIdea(Gson().fromJson(ideaJson, Idea::class.java))

        calculateAvg()
    }

    private fun initIdea(idea: Idea) {
        ideaId = idea.id
        contentInput.setText(idea.content)
        impactSpinner.setSelection(10 - idea.impact)
        easeSpinner.setSelection(10 - idea.ease)
        confidenceSpinner.setSelection(10 - idea.confidence)
    }

    private fun calculateAvg() {
        val impact = impactSpinner.selectedItem.toString().toFloat()
        val ease = easeSpinner.selectedItem.toString().toFloat()
        val confidence = confidenceSpinner.selectedItem.toString().toFloat()

        averageText.text = formatter.format((impact + ease + confidence) / 3)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewModel.idea.observe(this, Observer { resource ->
            if (resource != null && resource.isSuccessful) {
                onBackPressed()
            } else {
                Toast.makeText(this, resource?.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        const val IDEA_KEY = "intent.idea_key"

        fun start(context: Context, idea: Idea?) {
            val intent = Intent(context, CreateIdeaActivity::class.java)
            if (idea != null) {
                intent.putExtra(IDEA_KEY, Gson().toJson(idea))
            }
            context.startActivity(intent)
        }
    }
}
