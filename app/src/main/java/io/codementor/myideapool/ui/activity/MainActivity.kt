package io.codementor.myideapool.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import io.codementor.myideapool.R
import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.entity.IdeaAction
import io.codementor.myideapool.ui.adapter.IdeaAdapter
import io.codementor.myideapool.ui.fragment.IdeaDialogFragment
import io.codementor.myideapool.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.log

class MainActivity : AppCompatActivity(), IdeaAdapter.Callback {
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: IdeaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
        initViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart(PAGE_SIZE)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.logout) {
            viewModel.onLogout()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setIcon(R.drawable.lightbulb)

        adapter = IdeaAdapter(this)
        ideaList.layoutManager = LinearLayoutManager(this)
        ideaList.adapter = adapter

        addIdeaButton.setOnClickListener {
            startActivity(Intent(this, CreateIdeaActivity::class.java))
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewModel.user.observe(this, Observer {user ->
            if (user == null) {
                logout()
            }
        })

        viewModel.ideas.observe(this, Observer {pagedList ->
            if (pagedList != null && pagedList.size > 0) {
                placeholder.visibility = View.GONE
            } else {
                placeholder.visibility = View.VISIBLE
            }
            adapter.submitList(pagedList)
        })

        viewModel.idea.observe(this, Observer {resource ->
            if (resource != null && resource.isSuccessful) {
                viewModel.onStart(PAGE_SIZE)
            } else {
                Toast.makeText(this, resource?.message, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.ideaAction.observe(this, Observer {ideaAction ->
            if (ideaAction != null) {
                when (ideaAction.action) {
                    IdeaAction.ACTION_UPDATE -> CreateIdeaActivity.start(this, ideaAction.resource.data)
                    IdeaAction.ACTION_CREATE -> CreateIdeaActivity.start(this, null)
                    IdeaAction.ACTION_DELETE -> {
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle(getString(R.string.idea_action_delete_title))
                                .setMessage(getString(R.string.idea_action_delete_message))
                                .setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface, i: Int ->
                                    val idea = ideaAction.resource.data
                                    if (idea != null) viewModel.onDeleteIdea(idea)
                                }.setNegativeButton(android.R.string.cancel) { dialogInterface: DialogInterface, i: Int ->

                                }
                        builder.create().show()
                    }
                }
            }
        })

        viewModel.logout.observe(this, Observer {resource ->
            if (resource != null && resource.isSuccessful) {
                logout()
            } else {
                Toast.makeText(this, resource?.message, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.onCreate()
    }

    private fun logout() {
        finish()
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun onMore(idea: Idea) {
        IdeaDialogFragment.create(idea).show(supportFragmentManager, "")
    }

    companion object {
        const val PAGE_SIZE = 10
    }
}
