package io.codementor.myideapool.datasource

import android.arch.paging.PositionalDataSource
import io.codementor.myideapool.api.IdeaPoolApi
import io.codementor.myideapool.entity.Idea

class IdeaDataSource(private val ideaPoolApi: IdeaPoolApi) : PositionalDataSource<Idea>() {
    private var lastPage = 0

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Idea>) {
        val page = (params.startPosition / params.loadSize) + 1
        if (page != lastPage) {
            val ideas = load(page)
            callback.onResult(ideas)
        }
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Idea>) {
        val page = (params.requestedStartPosition / params.requestedLoadSize) + 1
        if (page != lastPage) {
            val ideas = load(page)
            callback.onResult(ideas, params.requestedStartPosition)
        }
    }

    private fun load(page: Int): List<Idea> {
        lastPage = page
        val response = ideaPoolApi.getIdeas(page).execute()
        return response.body() ?: ArrayList()
    }
}