package io.codementor.myideapool.repository

import io.codementor.myideapool.api.AuthApi
import io.codementor.myideapool.entity.*
import io.codementor.myideapool.helper.TokenHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository @Inject constructor(private val authApi: AuthApi, private val tokenHelper: TokenHelper) {
    fun login(email: String, password: String): Resource<Any> {
        val response = authApi.login(LoginRequest(email, password)).execute()

        return if (response.isSuccessful) {
            storeTokens(response.body())
            Resource.success(null)
        } else {
            Resource.error(response.message(), null)
        }
    }

    fun logout(): Resource<Any> {
        val token = tokenHelper.getToken()
        val refreshToken = tokenHelper.getRefreshToken()
        val response = authApi.logout(token, refreshToken).execute()

        return if (response.isSuccessful) {
            tokenHelper.clearTokens()
            Resource.success(null)
        } else {
            Resource.error(response.message(), null)
        }
    }

    fun signup(email: String, name: String, password: String): Resource<Any> {
        val response = authApi.signup(SignupRequest(email, name, password)).execute()

        return if (response.isSuccessful) {
            storeTokens(response.body())
            Resource.success(null)
        } else {
            Resource.error(response.message(), null)
        }
    }

    private fun storeTokens(tokenResponse: TokenResponse?) {
        val token = tokenResponse?.jwt ?: ""
        val refreshToken = tokenResponse?.refreshToken ?: ""
        tokenHelper.storeToken(token)
        tokenHelper.storeRefreshToken(refreshToken)
    }

    fun isLoggedIn(): Boolean = tokenHelper.hasToken()
}