package io.codementor.myideapool.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import io.codementor.myideapool.api.IdeaPoolApi
import io.codementor.myideapool.datasource.IdeaDataSource
import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.entity.IdeaRequest
import io.codementor.myideapool.entity.Resource
import io.codementor.myideapool.entity.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class IdeaPoolRepository @Inject constructor(private val ideaPoolApi: IdeaPoolApi) {
    fun getIdeasLiveData(pageSize: Int): LiveData<PagedList<Idea>> {
        val builder = LivePagedListBuilder<Int, Idea>(
                object : DataSource.Factory<Int, Idea>() {
                    override fun create(): DataSource<Int, Idea> {
                        return IdeaDataSource(ideaPoolApi)
                    }
                }, PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setPageSize(pageSize)
                        .setInitialLoadSizeHint(pageSize)
                        .build()
        )

        return builder.build()

    }
    fun createIdea(content: String, impact: Int, ease: Int, confidence: Int): Resource<Idea> {
        val response = ideaPoolApi.createIdea(IdeaRequest(content, impact, ease, confidence)).execute()

        return if (response.isSuccessful) {
            Resource.success(response.body())
        } else {
            Resource.error(response.message(), null)
        }
    }

    fun updateIdea(id: String, content: String, impact: Int, ease: Int, confidence: Int): Resource<Idea> {
        val response = ideaPoolApi.updateIdea(id, IdeaRequest(content, impact, ease, confidence)).execute()

        return if (response.isSuccessful) {
            Resource.success(response.body())
        } else {
            Resource.error(response.message(), null)
        }
    }

    fun deleteIdea(idea: Idea): Resource<Idea> {
        val response = ideaPoolApi.deleteIdea(idea.id).execute()

        return if (response.isSuccessful) {
            Resource.success(idea)
        } else {
            Resource.error(response.message(), null)
        }
    }

    fun getUser(): Resource<User> {
        val response = ideaPoolApi.getUser().execute()

        return if (response.isSuccessful) {
            Resource.success(response.body())
        } else {
            Resource.error(response.message(), null)
        }
    }
}