package io.codementor.myideapool.api

import io.codementor.myideapool.entity.*
import retrofit2.Call
import retrofit2.http.*

interface AuthApi {
    @POST("/access-tokens/refresh")
    fun refreshToken(@Body refreshTokenRequest: RefreshTokenRequest): Call<RefreshTokenResponse>

    @POST("/access-tokens")
    fun login(@Body loginRequest: LoginRequest): Call<TokenResponse>

    @DELETE("/access-tokens")
    fun logout(@Header("x-access-token") token: String, @Query("refresh_token") refreshToken: String): Call<Any>

    @POST("/users")
    fun signup(@Body signupRequest: SignupRequest): Call<TokenResponse>
}