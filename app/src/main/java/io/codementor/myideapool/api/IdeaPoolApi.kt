package io.codementor.myideapool.api

import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.entity.IdeaRequest
import io.codementor.myideapool.entity.User
import retrofit2.Call
import retrofit2.http.*

interface IdeaPoolApi {
    @GET("/me")
    fun getUser(): Call<User>

    @POST("/ideas")
    fun createIdea(@Body ideaRequest: IdeaRequest): Call<Idea>

    @DELETE("/ideas/{id}")
    fun deleteIdea(@Path("id") id: String): Call<Any>

    @GET("/ideas")
    fun getIdeas(@Query("page") page: Int): Call<MutableList<Idea>>

    @PUT("/ideas/{id}")
    fun updateIdea(@Path("id") id: String, @Body ideaRequest: IdeaRequest): Call<Idea>
}