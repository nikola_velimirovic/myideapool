package io.codementor.myideapool.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import io.codementor.myideapool.api.AuthApi
import io.codementor.myideapool.entity.RefreshTokenRequest

class TokenHelper (private val context: Context, private val authApi: AuthApi) {
    fun refreshToken(): String {
        val response = authApi.refreshToken(RefreshTokenRequest(getRefreshToken())).execute()

        val responseBody = response.body()
        val token = responseBody?.jwt ?: ""
        storeToken(token)
        return token
    }

    fun storeToken(token: String) = getPrefs().edit().putString(TOKEN_KEY, token).apply()

    fun storeRefreshToken(refreshToken: String) = getPrefs().edit().putString(REFRESH_TOKEN_KEY, refreshToken).apply()

    fun clearTokens() {
        storeToken("")
        storeRefreshToken("")
    }

    fun hasToken(): Boolean = getToken() != ""

    fun getToken(): String = getPrefs().getString(TOKEN_KEY, "")

    fun getRefreshToken(): String = getPrefs().getString(REFRESH_TOKEN_KEY, "")

    private fun getPrefs(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    companion object {
        const val TOKEN_KEY = "preferences.token_key"
        const val REFRESH_TOKEN_KEY = "preferences.refresh_token_key"
    }
}