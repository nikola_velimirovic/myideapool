package io.codementor.myideapool.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RefreshTokenRequest(@SerializedName("refresh_token") val refreshToken: String)

data class LoginRequest(@Expose val email: String,
                        @Expose val password: String)

data class LogoutRequest(@SerializedName("refresh_token") val refreshToken: String)

data class SignupRequest(@Expose val email: String,
                         @Expose val name: String,
                         @Expose val password: String)

data class IdeaRequest(@Expose val content: String,
                       @Expose val impact: Int,
                       @Expose val ease: Int,
                       @Expose val confidence: Int)