package io.codementor.myideapool.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RefreshTokenResponse(@Expose val jwt: String)

data class TokenResponse(@Expose val jwt: String,
                         @SerializedName("refresh_token") val refreshToken: String)
