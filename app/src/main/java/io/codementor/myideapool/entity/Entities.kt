package io.codementor.myideapool.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(@Expose val email: String,
                @Expose val name: String,
                @SerializedName("avatar_url") val avatarUrl: String)

data class Idea(@Expose val id: String,
                @Expose val content: String,
                @Expose val impact: Int,
                @Expose val ease: Int,
                @Expose val confidence: Int,
                @SerializedName("average_score") val averageScore: Float,
                @SerializedName("created_at") val createdAt: Long)

data class IdeaAction(val resource: Resource<Idea>, val action: Int) {
    companion object {
        const val ACTION_CREATE = 1
        const val ACTION_UPDATE = 2
        const val ACTION_DELETE = 3
    }
}

