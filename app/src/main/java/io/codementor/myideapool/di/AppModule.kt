package io.codementor.myideapool.di

import android.content.Context
import dagger.Module
import dagger.Provides
import io.codementor.myideapool.IdeaPoolApplication
import io.codementor.myideapool.api.AuthApi
import io.codementor.myideapool.api.IdeaPoolApi
import io.codementor.myideapool.helper.TokenHelper
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(val app: IdeaPoolApplication) {
    private val authApi: AuthApi = createAuthApi()
    private val tokenHelper: TokenHelper = TokenHelper(app, authApi)

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideTokenHelper(): TokenHelper = tokenHelper

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val client = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(TokenInterceptor())
                .build()

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideAuthApi(): AuthApi = authApi

    @Provides
    @Singleton
    fun provideIdeaPoolApi(retrofit: Retrofit): IdeaPoolApi = retrofit.create(IdeaPoolApi::class.java)

    private fun createAuthApi(): AuthApi {
        val client = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AuthApi::class.java)
    }

    inner class TokenInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            var modifiedRequest = request
            if (tokenHelper.hasToken()) {
                modifiedRequest = request.newBuilder()
                        .header(HEADER_ACCESS_TOKEN, tokenHelper.getToken())
                        .build()
            }

            var response = chain.proceed(modifiedRequest)
            if (response.code() == 401) {
                val newToken = tokenHelper.refreshToken()
                modifiedRequest = request.newBuilder()
                        .header(HEADER_ACCESS_TOKEN, newToken)
                        .build()
                response = chain.proceed(modifiedRequest)
            }
            return response
        }
    }

    companion object {
        const val HEADER_ACCESS_TOKEN = "x-access-token"

        const val BASE_URL = "https://small-project-api.herokuapp.com"
    }
}