package io.codementor.myideapool.di

import dagger.Component
import io.codementor.myideapool.datasource.IdeaDataSource
import io.codementor.myideapool.viewmodel.AuthViewModel
import io.codementor.myideapool.viewmodel.MainViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(viewModel: MainViewModel)
    fun inject(viewModel: AuthViewModel)
}