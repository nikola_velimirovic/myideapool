package io.codementor.myideapool

import android.app.Application
import io.codementor.myideapool.di.AppComponent
import io.codementor.myideapool.di.AppModule
import io.codementor.myideapool.di.DaggerAppComponent

class IdeaPoolApplication : Application() {
    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }
}