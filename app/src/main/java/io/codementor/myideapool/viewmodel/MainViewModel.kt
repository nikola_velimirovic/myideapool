package io.codementor.myideapool.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.codementor.myideapool.IdeaPoolApplication
import io.codementor.myideapool.entity.Idea
import io.codementor.myideapool.entity.IdeaAction
import io.codementor.myideapool.entity.Resource
import io.codementor.myideapool.entity.User
import io.codementor.myideapool.repository.AuthRepository
import io.codementor.myideapool.repository.IdeaPoolRepository
import kotlinx.coroutines.experimental.async
import javax.inject.Inject

class MainViewModel : ViewModel() {
    @Inject
    lateinit var ideaPoolRepository: IdeaPoolRepository
    @Inject
    lateinit var authRepository: AuthRepository

    val user = MutableLiveData<User>()
    val ideaAction = MutableLiveData<IdeaAction>()
    val idea = MutableLiveData<Resource<Idea>>()
    var ideas: LiveData<PagedList<Idea>>
    val logout = MutableLiveData<Resource<Any>>()

    private val ideasRequest = MutableLiveData<Int>()

    init {
        IdeaPoolApplication.component.inject(this)

        ideas = Transformations.switchMap(ideasRequest) {pageSize ->
            ideaPoolRepository.getIdeasLiveData(pageSize)
        }
    }

    fun onCreate() = async {
        var userData: User? = null
        if (authRepository.isLoggedIn()) {
            val resource = ideaPoolRepository.getUser()
            userData = resource.data
        }

        user.postValue(userData)
    }

    fun onStart(pageSize: Int) = async {
        ideasRequest.postValue(pageSize)
    }

    fun onLogout() = async {
        val resource = authRepository.logout()
        logout.postValue(resource)
    }

    fun onSaveIdea(id: String?, content: String, impact: Int, ease: Int, confidence: Int) = async {
        if (id == null) {
            val resource = ideaPoolRepository.createIdea(content, impact, ease, confidence)
            idea.postValue(resource)
        } else {
            val resource = ideaPoolRepository.updateIdea(id, content, impact, ease, confidence)
            idea.postValue(resource)
        }
    }

    fun onDeleteIdea(idea: Idea) = async {
        val resource = ideaPoolRepository.deleteIdea(idea)
        this@MainViewModel.idea.postValue(resource)
    }

    fun onDeleteIdeaAction(idea: Idea) = async {
        ideaAction.postValue(IdeaAction(Resource.success(idea), IdeaAction.ACTION_DELETE))
    }

    fun onUpdateIdeaAction(idea: Idea) = async {
        ideaAction.postValue(IdeaAction(Resource.success(idea), IdeaAction.ACTION_UPDATE))
    }
}