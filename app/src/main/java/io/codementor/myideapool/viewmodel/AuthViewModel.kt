package io.codementor.myideapool.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.codementor.myideapool.IdeaPoolApplication
import io.codementor.myideapool.entity.Resource
import io.codementor.myideapool.repository.AuthRepository
import kotlinx.coroutines.experimental.async
import javax.inject.Inject

class AuthViewModel : ViewModel() {
    @Inject
    lateinit var authRepository: AuthRepository

    val loginResult = MutableLiveData<Resource<Any>>()
    val signupResult = MutableLiveData<Resource<Any>>()

    init {
        IdeaPoolApplication.component.inject(this)
    }

    fun onLogin(email: String, password: String) = async {
        val resource = authRepository.login(email, password)
        loginResult.postValue(resource)
    }

    fun onSignup(email: String, name: String, password: String) = async {
        val resource = authRepository.signup(email, name, password)
        signupResult.postValue(resource)
    }
}